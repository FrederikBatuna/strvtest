### 🔥 Code quality

Using eslint + flow

### 🔥 Security

How do you store your customers' passwords? What about security of your customers' data?ù

- store password as hash
- firebase rules


### 🔥 Testability

91% coverage

### API structure and usability

look into docks (could be improved here using API Blueprint istead of apidoc)

### Development and deployment

How hard is it to run your project locally?
`npm run dev`

And how hard is it to deploy it?
`git push heroku master`

### New language features

Using babel + flow

### Documentation

look into readme for docs
