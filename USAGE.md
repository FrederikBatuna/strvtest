# FIREBASE

The firebase url is [https://strvtest-64863.firebaseio.com](https://strvtest-64863.firebaseio.com).

Only the custom token authetication is enabled,
so in order to use firebase service you must get the custom token from rest api `/users/login`,
then signin wiht firebase client library with `signInWithCustomToken(firebaseToken)`,
the token is valid for 1 hour;

The user must be authenticated to write and read.

the global data schema is:
```json
{
  "contacts": {
    "user-uuid": [
      {
        "name": "contact name",
        "email": "contact email"
      }
    ]
  }
}
```

Rules can be found [here](src/application/firebase-rules.json).

So every user can manipulate all their contacts at will,
access to other users data if forbidden.

example of creating a new contact:
```javascript
firebase.database()
  .ref(`https://strvtest-64863.firebaseio.com/contacts/${userUID}`)
  .push().set({
    "name": "John Doe",
    "email": "john@doe.com"
  });
```

# REST API USAGE

see it [here](https://strv-test-frederik.herokuapp.com/doc)

or

- clone the repo
- cd into clone dir
- `yarn`
- `npm run apidoc`
- open ./apidoc/index.html
