// @flow

import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
chai.use(chaiHttp);
import jwt from 'jsonwebtoken';
import express from 'express';
import * as firebase from 'firebase';
import { firebase as config } from '../src/config';
const firebaseapp = firebase.initializeApp(config);
import { FIREBASE_SERVICE_ACCOUNT_JSON, PORT, TOKEN_SECRET } from '../src/config';
import application from '../src/application';
import { firebaseAdmin, firebaseUser } from '../src/application/FirebaseAdmin';
import api from '../src/api';

var app, userUID;
async function setupServer() {
  const now = String((new Date).getTime());
  const fba = firebaseAdmin(FIREBASE_SERVICE_ACCOUNT_JSON);
  const fbu = firebaseUser(FIREBASE_SERVICE_ACCOUNT_JSON);
  app = await application({
    mongoURI: `mongodb://localhost/${now}`,
    firebaseAdmin: fba,
    firebaseUser: fbu,
    TOKEN_SECRET
  });
  const server = express();
  server.use(api({ app }));
  server.listen(PORT);
  return server;
}
const server = setupServer();

describe('api', () => {
  before(() => server);
  after(async () => {
    await app.firebaseAdmin.auth().deleteUser(userUID);
    await app.firebaseAdmin.database().ref(`contacts/${userUID}`).remove();
    await app.db.dropDatabase();
  });
  describe('/users', () => {
    describe('POST', () => {
      it('(201) creates a new user', async () => {
        const res = await chai.request(await server)
          .post('/users').send({ email: 'test@test.com', password: 'testtest' });
        expect(res).to.have.property('status', 201);
        expect(res.body).to.have.property('email', 'test@test.com');
        expect(res.body).to.have.property('uuid');
      });
      it('(409) errors if same user created twice', async () => {
        try {
          await chai.request(await server)
          .post('/users').send({ email: 'test@test.com', password: 'testtest' });
          throw new Error('shouldn\'t get here');
        } catch (e) {
          expect(e).to.have.property('status', 409);
        }
      });
      it('(422) errors if missing params', async () => {
        try {
          await chai.request(await server)
          .post('/users').send({ email: 'test@test.com' });
          throw new Error('shouldn\'t get here');
        } catch (e) {
          expect(e).to.have.property('status', 422);
        }
      });
    });
  });
  describe('/users/login', () => {
    describe('POST', () => {
      it('(200) gets a correct token', async () => {
        const res = await chai.request(await server)
          .post('/users/login').send({ email: 'test@test.com', password: 'testtest' });
          expect(res).to.have.property('status', 200);
        const token = jwt.decode(res.body.serverToken);
        expect(token).to.have.property('aud', 'https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit');
      });
      it('(422) errors if missing params', async () => {
        try {
          await chai.request(await server)
            .post('/users/login').send({ email: 'test@test.com' });
          throw new Error('shouldn\'t get here');
        } catch (e) {
          expect(e).to.have.property('status', 422);
        }
      });
      it('(401) errors if invalid login', async () => {
        try {
          await chai.request(await server)
            .post('/users/login').send({ email: 'test@test.com', password: 'xxxxxxxxxxxxx' });
          throw new Error('shouldn\'t get here');
        } catch (e) {
          expect(e).to.have.property('status', 401);
        }
      });
    });
  });
  describe('/users/:useruid/contacts', () => {
    describe('POST', () => {
      it('(201) creates a new contact', async () => {
        const res = await chai.request(await server)
          .post('/users/login').send({ email: 'test@test.com', password: 'testtest' });
        await firebaseapp.auth().signInWithCustomToken(res.body.firebaseToken);
        const token = jwt.decode(res.body.firebaseToken);
        userUID = token.uid;
        const contactCreated = await chai.request(await server)
          .post(`/users/${userUID}/contacts`)
          .set('Authorization', `Bearer ${res.body.serverToken}`)
          .send({ name: 'fred', email: 'gobi301@gmail.com' });
        expect(contactCreated).to.have.property('status', 201);
        expect(contactCreated.body).to.deep.equal({ name: 'fred', email: 'gobi301@gmail.com' });
      });
      it('(401) if invalid token', async () => {
        try {
          await chai.request(await server)
            .post(`/users/${userUID}/contacts`).send({ name: 'fred', email: 'gobi301@gmail.com' });
          throw new Error('shouldn\'t get here');
        } catch (e) {
          expect(e).to.have.property('status', 401);
        }
      });
    });
  });
  describe('firebase client sdk access', () => {
    it('wont let write to database without authetication', async () => {
      try {
        await firebaseapp.database().ref('contacts/').set({ contact1: { name: 'fred' } });
        throw new Error('shouldn\'t get here');
      } catch (e) {
        expect(e.code).to.equal('PERMISSION_DENIED');
      }
    });
    it('logs in with custom token', async () => {
      const res = await chai.request(await server)
        .post('/users/login').send({ email: 'test@test.com', password: 'testtest' });
      await firebaseapp.auth().signInWithCustomToken(res.body.firebaseToken);
      const token = jwt.decode(res.body.firebaseToken);
      userUID = token.uid;
    });
  });
});
