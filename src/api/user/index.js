// @flow

import { Router } from 'express';
import contact from './contact';
import type { Application } from '../../application';
import { object, string, ValidationError } from 'flow-validator';
import jwt from 'jsonwebtoken';

export default function user(app: Application): Router {
  const router = Router();

  /**
   * @api {post} /users/ Create new user
   * @apiName CreateUser
   * @apiGroup User
   *
   * @apiParam {String} email User email.
   * @apiParam {String} password User password.
   *
   * @apiSuccess (201) {String} email User email.
   * @apiSuccess (201) {String} uuid User uuid.
   * @apiError (409) {String} message Human-readable error message
   * @apiError (422) {Object} validation-error validation error recap
   *
   * @apiSuccessExample {json} Success-Response:
   *     HTTP/1.1 201 OK
   *     {
   *       "email": "test@test.com",
   *       "uuid": "xxxxxxxxxxxx"
   *     }
   *
   * @apiErrorExample {json} Error-Response:
   *     HTTP/1.1 409 Conflict
   *     {
   *       "message": "user with given email already exists"
   *     }
   */
  const createUserRequestSchema = object({
    email: string.isEmail(),
    password: string.minLength(6)
  });
  router.post('/', async (req, res) => {
    try {
      const createUserArgs = createUserRequestSchema.parse(req.body);
      const user = await app.userRepo.create(createUserArgs);
      res.status(201).json(user);
    } catch (e) {
      if (e.code === 11000) {
        return res.status(409).json({ message: 'user with given email already exists' });
      }
      if (e instanceof ValidationError) return res.status(422).json(e.toJSON());
      res.status(500).json({ message: e.message });
    }
  });

  /**
   * @api {post} /users/login Get Access Token
   * @apiName Login
   * @apiGroup User
   *
   * @apiParam {String} email User email.
   * @apiParam {String} password User password.
   *
   * @apiSuccess (200) {String} firebaseToken Firebase custom access token
   * @apiSuccess (200) {String} serverToken access token to create contact on server
   *
   * @apiError (401) {String} message Login failed message
   * @apiError (422) {Object} validation-error validation error recap
   *
   * @apiSuccessExample {json} Success-Response:
   *     HTTP/1.1 200 OK
   *     {
   *       "firebaseToken": "xxxxxxxxxxxxx",
   *       "serverToken": "xxxxxxxxxx",
   *       "uuid": "xxxxxx",
   *     }
   */
  router.post('/login', async (req, res) => { // using post here because query params would show up in server logs
    try {
      const { email, password } = createUserRequestSchema.parse(req.body);
      const user = await app.userRepo.login({ email, password });
      if (!user) return res.status(401).json({ message: 'invalid credentials' });
      const firebaseToken = await app.firebaseAdmin.auth().createCustomToken(user.uuid);
      const serverToken = jwt.sign({
        uid: user.uuid,
        aud: 'https://identitytoolkit.googleapis.com/google.identity.identitytoolkit.v1.IdentityToolkit',
        iss: 'firebase-adminsdk-ye71a@strvtest-64863.iam.gserviceaccount.com',
        sub: 'firebase-adminsdk-ye71a@strvtest-64863.iam.gserviceaccount.com'
      }, app.TOKEN_SECRET, { expiresIn: '1h' });
      res.json({ firebaseToken, serverToken, uuid: user.uuid });
    } catch (e) {
      if (e instanceof ValidationError) return res.status(422).json(e.toJSON());
      res.status(500).json({ message: e.message });
    }
  });

  router.use('/:useruid', async (req, res, next) => {
    try {
      const tokenString = (req.get('Authorization') || '').replace(/^Bearer /, '');
      const token = jwt.verify(tokenString, app.TOKEN_SECRET);
      res.locals.token = token;
      if (token.uid !== req.params.useruid) throw new Error('wrong user endpoint');
      next();
    } catch (e) {
      res.status(401).json({ message: e.message });
    }
  });

  router.use('/:useruid/contacts', contact(app));

  return router;
}
