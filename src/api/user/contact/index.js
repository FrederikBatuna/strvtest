// @flow

import { Router } from 'express';
import type { Application } from '../../../application';
import { objectType, ValidationError } from 'flow-validator';

export default function contact(app: Application): Router {
  const router = Router({ mergeParams: true });

  /**
   * @api {post} /users/:useruid/contacts Create new Contact
   * @apiName CreateContact
   * @apiGroup Contact
   *
   * @apiHeader Contact {Bearer JWT} Authorization jwt token got from login (serverToken)
   * @apiHeaderExample {Bearer JWT} Header-Example:
   *     Bearer xxxxxxxxxxxxxxxxxxx
   *
   * @apiParam {json} contact Custom contact data.
   *
   * @apiSuccess (201) {Object} contact Newly created Contact
   *
   * @apiSuccessExample Success-Response:
   *     HTTP/1.1 201 OK
   *     {
   *       "name": "fred",
   *       "email": "gobi301@gmail.com"
   *     }
   */
  router.post('/', async (req, res) => {
    try {
      const { useruid } = req.params;
      const contactData = objectType.validate(req.body);
      const appended = await app.contactRepo.create({ ownerUID: useruid, data: contactData });
      res.status(201).json(appended);
    } catch (e) {
      if (e instanceof ValidationError) return res.status(422).json(e.toJSON());
      res.status(500).json({ message: e.message });
    }
  });

  return router;
}
