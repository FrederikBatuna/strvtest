// @flow

import type { Application } from '../application';
import express, { Router } from 'express';
import bodyParser from 'body-parser';
import user from './user';

export default function api({ app }: { app: Application }): Router {
  const router = Router();
  router.use(bodyParser.json());
  router.use('/users', user(app));
  router.use('/doc', express.static('apidoc'));
  return router;
}
