// @flow

export const firebase = {
  apiKey: 'AIzaSyBhaWTkhA7ZzRvdkBLe5JONn3Ta9jD9ZXg',
  authDomain: 'strvtest-64863.firebaseapp.com',
  databaseURL: 'https://strvtest-64863.firebaseio.com',
  storageBucket: 'strvtest-64863.appspot.com',
  messagingSenderId: '504657384435'
};

import type { FirebaseServiceAccountJSON } from './application/FirebaseAdmin';

export const FIREBASE_SERVICE_ACCOUNT_JSON: FirebaseServiceAccountJSON = (() => {
  if (!process.env.FIREBASE_SERVICE_ACCOUNT_JSON) throw new Error('missing firebase accoun service conf');
  return JSON.parse(process.env.FIREBASE_SERVICE_ACCOUNT_JSON);
})();

export const PORT = process.env.PORT || 5000;

export const MONGODB_URI = process.env.MONGODB_URI || 'mongodb://localhost/contactbook';

export const TOKEN_SECRET = process.env.TOKEN_SECRET || 'qwerty';
