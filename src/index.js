// @flow

import application from './application';
import api from './api';
import { firebaseAdmin, firebaseUser } from './application/FirebaseAdmin';
import { PORT, MONGODB_URI, FIREBASE_SERVICE_ACCOUNT_JSON, TOKEN_SECRET } from './config';
import express from 'express';

export const server = (async () => {
  try {
    const fba = firebaseAdmin(FIREBASE_SERVICE_ACCOUNT_JSON);
    const fbu = firebaseUser(FIREBASE_SERVICE_ACCOUNT_JSON);
    const app = await application({
      mongoURI: MONGODB_URI,
      firebaseAdmin: fba,
      firebaseUser: fbu,
      TOKEN_SECRET,
      connectingDatabase: uri => console.log(`connecting to database ${uri} ...`), // eslint-disable-line no-console
      connectedDatabase: uri => console.log(`connected to database ${uri} ✓`) // eslint-disable-line no-console
    });
    const server = express();
    server.use(api({ app }));
    console.log(`Starting server on port ${PORT} ...`); // eslint-disable-line no-console
    server.listen(PORT, () => console.log(`Server listening on port ${PORT} ✓`)); // eslint-disable-line no-console
    return server;
  } catch (e) {
    console.error(e); // eslint-disable-line no-console
    process.exit(1);
  }
})();
