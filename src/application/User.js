// @flow

import crypto from 'crypto';
import { Database, ObjectId } from 'mongodb';

export class UserRepo {
  db: Database;
  constructor({ db }: { db: Database }) { this.db = db; }
  static async create(args: { db: Database }): Promise<UserRepo> {
    await args.db.collection('users').ensureIndex({ 'email': 1 }, { unique: true });
    return new UserRepo(args);
  }
  async create({ email, password }: { email: string, password: string }): Promise<User> {
    const uuid = ObjectId();
    await this.db.collection('users').insertOne({
      _id: uuid,
      email: email,
      passwordHash: hashPassword(password)
    });
    return new User({ email, uuid: String(uuid) });
  }
  async login({ email, password }: { email: string, password: string }): Promise<?User> {
    const data = await this.db.collection('users')
      .findOne({ email, passwordHash: hashPassword(password) });
    if (data) return new User({ email: data.email, uuid: String(data._id) });
    return null;
  }
}

export class User {
  email: string;
  uuid: string;
  constructor({ email, uuid }: { email: string, uuid: string }) {
    this.email = email;
    this.uuid = uuid;
  }
}

function hashPassword(pwd: string): string {
  return crypto.createHash('md5').update(pwd).digest('hex');
}
