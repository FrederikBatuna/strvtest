// @flow

export class ContactRepo {
  firebase: (uid: string) => Object;
  constructor({ firebase }: { firebase: Object }) {
    this.firebase = firebase;
  }
  async create({ ownerUID, data }: { ownerUID: string, data: Object }): Promise<Contact> {
    await this.firebase(ownerUID).database()
      .ref(`contacts/${ownerUID}`).push().set(data);
    return new Contact(data);
  }
}

export class Contact {
  constructor(data: Object) {
    Object.assign(this, data);
  }
}
