// @flow

import admin from 'firebase-admin';

export type FirebaseServiceAccountJSON = {
  "type": "service_account",
  "project_id": string,
  "private_key_id": string,
  "private_key": string,
  "client_email": string,
  "client_id": string,
  "auth_uri": string,
  "token_uri": string,
  "auth_provider_x509_cert_url": string,
  "client_x509_cert_url": string
}

export function firebaseAdmin(serviceAccount: FirebaseServiceAccountJSON) {
  return admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: 'https://strvtest-64863.firebaseio.com'
  });
}

export function firebaseUser(serviceAccount: FirebaseServiceAccountJSON): (uid: string) => Object {
  const pool = {};
  return uid => {
    if (uid in pool) return pool[uid];
    return pool[uid] = admin.initializeApp({
      credential: admin.credential.cert(serviceAccount),
      databaseURL: 'https://strvtest-64863.firebaseio.com',
      databaseAuthVariableOverride: { uid }
    }, uid);
  };
}
