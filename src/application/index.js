// @flow

import { MongoClient, Database } from 'mongodb';
import { UserRepo } from './User';
import { ContactRepo } from './Contact';

type ApplicationArgs = {
  mongoURI: string,
  firebaseAdmin: Object,
  firebaseUser: (uuid: string) => Object,
  TOKEN_SECRET: string,
  connectingDatabase?: (uri: string) => mixed,
  connectedDatabase?: (uri: string) => mixed
}

export type Application = {
  db: Database,
  firebaseAdmin: Object,
  firebaseUser: (uuid: string) => Object,
  TOKEN_SECRET: string,
  userRepo: UserRepo,
  contactRepo: ContactRepo,
}

export default async function application(args: ApplicationArgs): Promise<Application> {
  if (args.connectingDatabase) args.connectingDatabase(args.mongoURI);
  const db = await MongoClient.connect(args.mongoURI);
  if (args.connectedDatabase) args.connectedDatabase(args.mongoURI);
  const userRepo = await UserRepo.create({ db });
  const contactRepo = new ContactRepo({ firebase: args.firebaseUser });
  return {
    db,
    firebaseAdmin: args.firebaseAdmin,
    firebaseUser: args.firebaseUser,
    userRepo, contactRepo,
    TOKEN_SECRET: args.TOKEN_SECRET
  };
}
