# Adress Book REST API

## Installation

- you will need node and yarn
- git clone
- cd into dir
- `yarn`

## Development

`npm run dev`

## Testing

start local mongodb isntance

`npm test`

to see coverage

`npm run coverage`

## USAGE

[usage](USAGE.md)

## CONFIGURATION

to configure the server, these environmental variables are needed:

- FIREBASE_SERVICE_ACCOUNT_JSON (download the file and set env variable with its content)
- PORT (the port where the server will be listening)
- MONGODB_URI (on heroku it's already set by mongodb addon)
- TOKEN_SECRET (used for json web token encryption)

## Technical documentation

to see internal code documentation

`npm run doc:server`
