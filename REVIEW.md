# Test project review

Overall, a very nice projects. You clearly understood project requirements and implemented them all. The project is well structured, documented and tested. No major issues found, just few minor.

## Minor issues

- Weak hash algorithm & unsalted passwords 
> MD5 hash algorithm is not an ideal choice for password hashes. Since the hashes are not salted they are susceptible to rainbow table attacks and brute-forcing the passwords is quite cheap. Use bcrypt or argon2 instead.

- Error handling in every route 
> Each route currently contains `try/catch` blocks. It might be worth to keep error handling at one place - e.g. error handling middleware.

- Inconsistent file naming 
> Usually, it's better to use the same naming pattern across the whole project, ideally lowercase. Some unix systems may have different case-sensitive settings. Keeping file names in lowercase will prevent issues that might occure during deployment phase.

> No loggging library 
> It's generally recommended to use a logging library instead of `console.log` - e.g. bunyan, pino. Both libraries generate structured logs that are good for making queries later.

## Improvements suggestions

- Keep all configuration variables in config 
> Firebase URLs and emails might go to the `config.js` file as well.

## Nice parts 
- Eslint & Flow typing 
- Nice documentation & description (requriments, usage, readme) 
- Firebase rules set

Well done.

Two thumbs up	Development process
Thumbs up	Architecture, Code quality, Infrastructure, Project requirements
Mixed icon	Application security